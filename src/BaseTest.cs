using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using uMod.Common;

namespace uMod.Mock
{
    public class TestScope
    {
        public IBatchPluginLoader PluginLoader;
        public static object LogLock = new object();
        public static StringBuilder Log = new StringBuilder();

        public IModule Module;

        public bool pluginLoaderRegistered;
        public bool TestPluginsFound;
        public readonly List<Type> TestPlugins = new List<Type>();
        public bool TestExtensionsFound;
        public readonly List<Type> TestExtensions = new List<Type>();
        public bool TestLibrariesFound;
        public readonly List<Type> TestLibraries = new List<Type>();
        public bool TestCompilationsFound;
        public readonly Dictionary<string, List<string>> TestCompilations = new Dictionary<string, List<string>>();
        public bool TestEnvironmentFound;
        public readonly IDictionary<string, string> TestEnvironment = new Dictionary<string, string>();
        public bool TestAppsFound;
        public readonly List<string> TestApps = new List<string>();
        public readonly List<string> LoadApps = new List<string>();
        public ServerScope ServerScope;

        public int Total;
        public List<string> RunTests = new List<string>();

        public TestScope(ServerScope scope)
        {
            ServerScope = scope;
        }
    }

    public abstract class BaseTest
    {
        public static BaseTest CurrentTest;
        public TestContext TestContext { get; set; }

        protected readonly string AppDirectory = Path.Combine(Environment.CurrentDirectory, "umod", "apps");
        protected readonly string DataDirectory = Path.Combine(Environment.CurrentDirectory, "umod", "data");

        public static Dictionary<string, TestScope> TestScopes = new Dictionary<string, TestScope>();

        public TestScope CurrentScope;

        public Assertion Assert { get; private set; }

        public TestScope CreateTestScope(ServerScope serverScope)
        {
            TestScope scope = new TestScope(serverScope);

            foreach (MethodInfo methodInfo in GetType().GetMethods())
            {
                TestMethodAttribute testMethod = methodInfo.GetCustomAttribute<TestMethodAttribute>();
                if (testMethod != null)
                {
                    scope.Total++;
                }

                DataTestMethodAttribute dataTestMethod = methodInfo.GetCustomAttribute<DataTestMethodAttribute>();
                if (dataTestMethod != null)
                {
                    scope.Total++;
                }
            }

            return scope;
        }

        [TestInitialize]
        public virtual void SetupTest()
        {
            if (!TestScopes.TryGetValue(TestContext.FullyQualifiedTestClassName, out CurrentScope))
            {
                CurrentScope = CreateTestScope(GetServerScope());
                Assert = new Assertion(CurrentScope);
                TestScopes.Add(TestContext.FullyQualifiedTestClassName, CurrentScope);
            }

            if (Assert == null)
            {
                Assert = new Assertion(CurrentScope);
            }

            CurrentTest = this;
            SetupApps();
            SetupCompilations();

            CreateMockServer();

            ClearLog();
            Register();
            SetupLibraries();
            SetupPlugins();
        }

        [TestCleanup]
        public virtual void TeardownTest()
        {
            if (!CurrentScope.RunTests.Contains(TestContext.TestName))
            {
                CurrentScope.RunTests.Add(TestContext.TestName);
            }
            RestoreLog();
            if (CurrentScope.ServerScope == ServerScope.PerMethod || CurrentScope.RunTests.Count >= CurrentScope.Total)
            {
                CurrentScope.Module.OnShutdown();
                CurrentScope.Module = null;
            }
            TeardownCompilations();
        }

        /// <summary>
        /// Recursively delete all apps
        /// </summary>
        public virtual void Clean()
        {
            DirectoryInfo AppDirectory = new DirectoryInfo(this.AppDirectory);
            AppDirectory.RecursiveDelete();
        }

        public abstract IModule LoadModule();

        /// <summary>
        /// Create a mock server
        /// </summary>
        /// <param name="developBranch"></param>
        /// <param name="info"></param>
        public virtual void CreateMockServer(bool developBranch = false, IInitializationInfo info = null)
        {
            /*
            string agentFile = (Environment.OSVersion.Platform == PlatformID.Win32NT) ? "uMod.exe" : "uMod";

            if (!Directory.Exists(AppDirectory))
            {
                Directory.CreateDirectory(AppDirectory);
            }

            string agentPath = Path.Combine(AppDirectory, agentFile);

            if (!File.Exists(agentPath))
            {
                ProcessHandler.DownloadAgent(AppDirectory, developBranch);
                if (File.Exists(agentPath) && Environment.OSVersion.Platform == PlatformID.Unix)
                {
                    ProcessHandler.Exec($"chmod 744 \"{agentPath}\"");
                }
            }
            */

            DeleteFiles(new[]
            {
                "umod.users.data",
                "umod.group.data",
                "umod.lang.data",
                "umod.users.json",
                "umod.group.json",
                "umod.lang.json",
                "umod.db"
            });

            if (CurrentScope.ServerScope == ServerScope.PerMethod || CurrentScope.Module == null)
            {
                CurrentScope.Module = LoadModule();

                CurrentScope.Module.Application.AddConverter<GamePlayer, GamePlayerIdentity>(gamePlayer => gamePlayer.Identity);
                CurrentScope.Module.Application.AddConverter<GamePlayerIdentity, GamePlayer>(gamePlayerIdentity => gamePlayerIdentity.GamePlayer);

                CurrentScope.Module.CallHook("InitLogging");
            }
        }

        private void DeleteFiles(IEnumerable<string> files)
        {
            foreach (string fileName in files)
            {
                string path = Path.Combine(DataDirectory, fileName);
                if (File.Exists(path))
                {
                    try
                    {
                        File.Delete(path);
                    }
                    catch (Exception)
                    {
                        // Ignored
                    }
                }
            }
        }

        public virtual void Register()
        {
        }

        private static StringBuilder _oldLog;

        internal static string GetLog()
        {
            lock (TestScope.LogLock)
            {
                return TestScope.Log.ToString();
            }
        }

        /// <summary>
        /// Clear log previous state
        /// </summary>
        protected void ClearLog()
        {
            lock (TestScope.LogLock)
            {
                _oldLog = TestScope.Log;
                TestScope.Log = new StringBuilder();
            }
        }

        /// <summary>
        /// Restore log to previous state
        /// </summary>
        protected void RestoreLog()
        {
            lock (TestScope.LogLock)
            {
                TestScope.Log = _oldLog;
            }
        }

        private readonly List<string> compilationsList = new List<string>();

        protected BaseTest Compile(params string[] paths)
        {
            return CompileTo(string.Empty, paths);
        }

        protected BaseTest CompileTo(string root, params string[] paths)
        {
            DirectoryInfo solutionDirInfo = new DirectoryInfo(Environment.CurrentDirectory).TryGetSolutionDirectory();
            string solutionDir = solutionDirInfo.FullName;

            string pluginPath = Path.Combine(Environment.CurrentDirectory, "umod", "plugins");
            if (!string.IsNullOrEmpty(root))
            {
                pluginPath = Path.Combine(pluginPath, root);
                if (!Directory.Exists(pluginPath))
                {
                    Directory.CreateDirectory(pluginPath);
                }
            }

            foreach (string path in paths)
            {
                if (string.IsNullOrEmpty(path))
                {
                    continue;
                }
                string fullPath = Path.GetFullPath(Path.Combine(solutionDir, path));
                // List of full paths for each individual file
                List<string> files = new List<string>();

                if (Directory.Exists(fullPath))
                {
                    files.AddRange(Directory.GetFiles(fullPath, "*.cs"));
                }
                else
                {
                    files.Add(fullPath);
                }

                foreach (string filePath in files)
                {
                    FileInfo fileInfo = new FileInfo(filePath);
                    string destPath = Path.Combine(pluginPath, fileInfo.Name);
                    if (File.Exists(destPath))
                    {
                        TryFileCallback(delegate
                        {
                            File.Delete(destPath);
                        });
                    }

                    TryFileCallback(delegate
                    {
                        File.Copy(filePath, destPath, true);
                    });

                    if (!compilationsList.Contains(destPath))
                    {
                        compilationsList.Add(destPath);
                    }
                }
            }

            return this;
        }

        private void TryFileCallback(Action callback)
        {
            int retries = 0;
            while (retries < 4)
            {
                try
                {
                    callback.Invoke();
                }
                catch (IOException)
                {
                    Thread.Sleep(10);
                }

                retries++;
            }
        }

        protected void SetupCompilations()
        {
            compilationsList.Clear();
            foreach (KeyValuePair<string, List<string>> kvp in GetTestCompilations())
            {
                CompileTo(kvp.Key, kvp.Value.ToArray());
            }

            foreach (KeyValuePair<string, List<string>> kvp in GetTestMethodCompilations())
            {
                CompileTo(kvp.Key, kvp.Value.ToArray());
            }
        }

        protected void TeardownCompilations()
        {
            foreach (string compilation in compilationsList)
            {
                if (File.Exists(compilation))
                {
                    File.Delete(compilation);
                }
            }
            compilationsList.Clear();
        }

        protected void SetupApps()
        {
            CurrentScope.LoadApps.Clear();
            CurrentScope.LoadApps.AddRange(GetTestApps());
            CurrentScope.LoadApps.AddRange(GetTestMethodApps());
        }

        protected ServerScope GetServerScope()
        {
            ServerAttribute serverAttribute = GetType().GetCustomAttribute<ServerAttribute>();
            if (serverAttribute != null)
            {
                return serverAttribute.ServerScope;
            }

            return ServerScope.PerTest;
        }

        protected void SetupLibraries()
        {
            Type[] libraryTypes = GetTestMethodLibraries();
            if (libraryTypes.Length > 0)
            {
                foreach (Type libraryType in libraryTypes)
                {
                    ILibrary library =
                        CurrentScope.Module.Application.MakeParams<ILibrary>(libraryType, CurrentScope.Module.Application);
                    CurrentScope.Module.Libraries.Register(libraryType.Name, library);
                }
            }

            libraryTypes = GetTestLibraries();
            if (libraryTypes.Length > 0)
            {
                foreach (Type libraryType in libraryTypes)
                {
                    ILibrary library =
                        CurrentScope.Module.Application.MakeParams<ILibrary>(libraryType, CurrentScope.Module.Application);
                    CurrentScope.Module.Libraries.Register(libraryType.Name, library);
                }
            }
        }

        protected void SetupPlugins()
        {
            if (CurrentScope.pluginLoaderRegistered)
            {
                CurrentScope.PluginLoader.Reset();
            }

            Type[] pluginTypes = GetPluginTypes();
            if (pluginTypes.Length > 0)
            {
                CurrentScope.PluginLoader.RegisterPlugin(pluginTypes);
            }

            if (!CurrentScope.pluginLoaderRegistered || CurrentScope.ServerScope == ServerScope.PerMethod)
            {
                CurrentScope.Module.Extensions.RegisterLoader(CurrentScope.PluginLoader);
                CurrentScope.pluginLoaderRegistered = true;
            }

            if (CurrentScope.PluginLoader.CorePlugins.Length > 0)
            {
                CurrentScope.Module.Plugins.LoadAll(true);
            }
        }

        /// <summary>
        /// Gets all types to be loaded for the test
        /// </summary>
        /// <returns></returns>
        protected virtual Type[] GetPluginTypes()
        {
            List<Type> pluginTypes = new List<Type>(GetTestMethodPlugins());

            Type[] testPlugins = GetTestPlugins();
            if (testPlugins.Length > 0)
            {
                pluginTypes.AddRange(testPlugins);
            }

            return pluginTypes.ToArray();
        }

        protected void SetupExtensions()
        {
            Type[] extensionTypes = GetExtensionTypes();
            foreach (Type testExtension in extensionTypes)
            {
                CurrentScope.Module.Extensions.Load(testExtension);
            }
        }

        protected string[] GetTestApps()
        {
            if (CurrentScope.TestAppsFound)
            {
                return CurrentScope.TestApps.ToArray();
            }

            IEnumerable<ApplicationAttribute> appAttributes = GetType().GetCustomAttributes<ApplicationAttribute>(true);
            foreach (ApplicationAttribute appAttribute in appAttributes)
            {
                CurrentScope.TestApps.AddRange(appAttribute.AppNames);
            }

            CurrentScope.TestAppsFound = true;

            return CurrentScope.TestApps.ToArray();
        }

        protected string[] GetTestMethodApps()
        {
            List<string> appTypes = new List<string>();
            MethodInfo testMethod =
                GetType().GetMethod(TestContext.TestName, BindingFlags.Instance | BindingFlags.Public);

            if (testMethod != null)
            {
                IEnumerable<ApplicationAttribute> appAttributes = testMethod.GetCustomAttributes<ApplicationAttribute>(true);
                foreach (ApplicationAttribute appAttribute in appAttributes)
                {
                    appTypes.AddRange(appAttribute.AppNames);
                }
            }

            return appTypes.ToArray();
        }

        protected virtual Type[] GetExtensionTypes()
        {
            List<Type> extensionTypes = new List<Type>(GetTestMethodExtensions());

            Type[] testExtensionTypes = GetTestExtensions();
            if (testExtensionTypes.Length > 0)
            {
                extensionTypes.AddRange(testExtensionTypes);
            }

            return extensionTypes.ToArray();
        }

        protected Type[] GetTestExtensions()
        {
            if (CurrentScope.TestExtensionsFound)
            {
                return CurrentScope.TestExtensions.ToArray();
            }

            IEnumerable<ExtensionAttribute> extensionAttributes = GetType().GetCustomAttributes<ExtensionAttribute>(true);
            foreach (ExtensionAttribute extensionAttribute in extensionAttributes)
            {
                CurrentScope.TestExtensions.AddRange(extensionAttribute.ExtensionTypes);
            }

            CurrentScope.TestExtensionsFound = true;

            return CurrentScope.TestExtensions.ToArray();
        }

        protected Type[] GetTestMethodExtensions()
        {
            List<Type> extensionTypes = new List<Type>();
            MethodInfo testMethod =
                GetType().GetMethod(TestContext.TestName, BindingFlags.Instance | BindingFlags.Public);

            if (testMethod != null)
            {
                IEnumerable<ExtensionAttribute> extensionAttributes = testMethod.GetCustomAttributes<ExtensionAttribute>(true);
                foreach (ExtensionAttribute extensionAttribute in extensionAttributes)
                {
                    extensionTypes.AddRange(extensionAttribute.ExtensionTypes);
                }
            }

            return extensionTypes.ToArray();
        }

        protected Type[] GetTestPlugins()
        {
            if (CurrentScope.TestPluginsFound)
            {
                return CurrentScope.TestPlugins.ToArray();
            }

            IEnumerable<PluginAttribute> pluginAttributes = GetType().GetCustomAttributes<PluginAttribute>(true);
            if (pluginAttributes.Any())
            {
                foreach (PluginAttribute pluginAttribute in pluginAttributes)
                {
                    CurrentScope.TestPlugins.AddRange(pluginAttribute.PluginTypes);
                }
            }

            CurrentScope.TestPluginsFound = true;

            return CurrentScope.TestPlugins.ToArray();
        }

        protected Type[] GetTestMethodPlugins()
        {
            List<Type> pluginTypes = new List<Type>();
            MethodInfo testMethod =
                GetType().GetMethod(TestContext.TestName, BindingFlags.Instance | BindingFlags.Public);

            if (testMethod != null)
            {
                IEnumerable<PluginAttribute> pluginAttributes = testMethod.GetCustomAttributes<PluginAttribute>(true);
                if (pluginAttributes.Any())
                {
                    foreach (PluginAttribute pluginAttribute in pluginAttributes)
                    {
                        pluginTypes.AddRange(pluginAttribute.PluginTypes);
                    }
                }
            }

            return pluginTypes.ToArray();
        }

        protected Type[] GetTestLibraries()
        {
            if (CurrentScope.TestLibrariesFound)
            {
                return CurrentScope.TestLibraries.ToArray();
            }

            IEnumerable<LibraryAttribute> libraryAttributes = GetType().GetCustomAttributes<LibraryAttribute>(true);
            if (libraryAttributes.Any())
            {
                foreach (LibraryAttribute libraryAttribute in libraryAttributes)
                {
                    CurrentScope.TestLibraries.AddRange(libraryAttribute.LibraryTypes);
                }
            }

            CurrentScope.TestLibrariesFound = true;

            return CurrentScope.TestLibraries.ToArray();
        }

        protected Type[] GetTestMethodLibraries()
        {
            List<Type> libraryTypes = new List<Type>();
            MethodInfo testMethod =
                GetType().GetMethod(TestContext.TestName, BindingFlags.Instance | BindingFlags.Public);

            if (testMethod != null)
            {
                IEnumerable<LibraryAttribute> libraryAttributes = testMethod.GetCustomAttributes<LibraryAttribute>(true);
                if (libraryAttributes.Any())
                {
                    foreach (LibraryAttribute libraryAttribute in libraryAttributes)
                    {
                        libraryTypes.AddRange(libraryAttribute.LibraryTypes);
                    }
                }
            }

            return libraryTypes.ToArray();
        }

        protected IDictionary<string, string> GetEnvironment()
        {
            IDictionary<string, string> testEnvironment = GetTestEnvironment();
            IDictionary<string, string> testMethodEnvironment = GetTestMethodEnvironment();

            return testEnvironment.Union(testMethodEnvironment).ToDictionary(k => k.Key, v => v.Value);
        }

        protected IDictionary<string, string> GetTestEnvironment()
        {
            if (CurrentScope.TestEnvironmentFound)
            {
                return CurrentScope.TestEnvironment;
            }

            IEnumerable<EnvironmentAttribute> environmentAttributes = GetType().GetCustomAttributes<EnvironmentAttribute>(true);
            if (environmentAttributes.Any())
            {
                foreach (EnvironmentAttribute environmentAttribute in environmentAttributes)
                {
                    CurrentScope.TestEnvironment.Add(environmentAttribute.Name, environmentAttribute.Value);
                }
            }

            CurrentScope.TestEnvironmentFound = true;

            return CurrentScope.TestEnvironment;
        }

        protected IDictionary<string, string> GetTestMethodEnvironment()
        {
            Dictionary<string, string> environment = new Dictionary<string, string>();
            MethodInfo testMethod =
                GetType().GetMethod(TestContext.TestName, BindingFlags.Instance | BindingFlags.Public);

            if (testMethod != null)
            {
                IEnumerable<EnvironmentAttribute> environmentAttributes = testMethod.GetCustomAttributes<EnvironmentAttribute>(true);
                if (environmentAttributes.Any())
                {
                    foreach (EnvironmentAttribute environmentAttribute in environmentAttributes)
                    {
                        environment.Add(environmentAttribute.Name, environmentAttribute.Value);
                    }
                }
            }

            return environment;
        }

        protected Dictionary<string, List<string>> GetTestCompilations()
        {
            if (CurrentScope.TestCompilationsFound)
            {
                return CurrentScope.TestCompilations;
            }

            IEnumerable<CompileAttribute> compileAttributes = GetType().GetCustomAttributes<CompileAttribute>(true);
            if (compileAttributes.Any())
            {
                foreach (CompileAttribute compileAttribute in compileAttributes)
                {
                    if (!CurrentScope.TestCompilations.TryGetValue(compileAttribute.Root, out List<string> paths))
                    {
                        CurrentScope.TestCompilations.Add(compileAttribute.Root, paths = new List<string>());
                    }
                    paths.AddRange(compileAttribute.Paths);
                }
            }

            CurrentScope.TestCompilationsFound = true;

            return CurrentScope.TestCompilations;
        }

        protected Dictionary<string, List<string>> GetTestMethodCompilations()
        {
            Dictionary<string, List<string>> compilationFiles = new Dictionary<string, List<string>>();
            MethodInfo testMethod =
                GetType().GetMethod(TestContext.TestName, BindingFlags.Instance | BindingFlags.Public);

            if (testMethod != null)
            {
                IEnumerable<CompileAttribute> compileAttributes = testMethod.GetCustomAttributes<CompileAttribute>(true);
                if (compileAttributes.Any())
                {
                    foreach (CompileAttribute compileAttribute in compileAttributes)
                    {
                        if (!compilationFiles.TryGetValue(compileAttribute.Root, out List<string> paths))
                        {
                            compilationFiles.Add(compileAttribute.Root, paths = new List<string>());
                        }

                        paths.AddRange(compileAttribute.Paths);
                    }
                }
            }

            return compilationFiles;
        }

        protected T GetPlugin<T>() where T : class, IPlugin
        {
            return (T)GetPlugin(typeof(T));
        }

        protected abstract IPlugin GetPlugin(Type type);

        protected T GetLibrary<T>(string name = null) where T : class, ILibrary
        {
            return CurrentScope.Module.Libraries.Get<T>(name);
        }

        protected ILibrary GetLibrary(string name)
        {
            return CurrentScope.Module.Libraries.Get(name);
        }

        protected T GetExtension<T>() where T : class, IExtension
        {
            return CurrentScope.Module.Extensions.Get<T>();
        }

        protected IExtension GetExtension(Type type)
        {
            return CurrentScope.Module.Extensions.Get(type.Name);
        }
    }
}
