﻿using System;
using System.IO;
using System.Linq;

namespace uMod.Mock
{
    public static class ExtensionMethods
    {
        public static DirectoryInfo TryGetSolutionDirectory(this DirectoryInfo directory)
        {
            while (directory != null && !directory.GetFiles("*.sln").Any())
            {
                directory = directory.Parent;
            }
            return directory;
        }

        public static void CopyTo(this DirectoryInfo directory, string destFolder)
        {
            foreach (FileInfo file in directory.GetFiles("*.data", SearchOption.AllDirectories))
            {
                string modulePath = Directory.GetParent(file.FullName).FullName;
                string moduleName = Directory.GetParent(file.FullName).Name;
                Directory.CreateDirectory(Path.Combine(destFolder, moduleName));
                foreach (string subFolder in Directory.GetDirectories(modulePath, "*", SearchOption.AllDirectories))
                {
                    string dest = subFolder.Replace(modulePath, Path.Combine(destFolder, moduleName));
                    Directory.CreateDirectory(dest);
                    DirectoryInfo subFolderInfo = new DirectoryInfo(subFolder);
                    subFolderInfo.CopyTo(dest);
                }
                foreach (string allFiles in Directory.GetFiles(modulePath, "*.*", SearchOption.AllDirectories))
                {
                    string dest = allFiles.Replace(modulePath, Path.Combine(destFolder, moduleName));
                    File.Copy(allFiles, dest, true);
                }
            }
        }

        public static void RecursiveDelete(this DirectoryInfo directory)
        {
            if (!directory.Exists)
            {
                return;
            }

            foreach (DirectoryInfo dir in directory.EnumerateDirectories())
            {
                RecursiveDelete(dir);
            }

            FileInfo[] files = directory.GetFiles();
            foreach (FileInfo file in files)
            {
                file.IsReadOnly = false;
                try
                {
                    file.Delete();
                }
                catch (Exception)
                {
                    // Ignored
                }
            }
        }
    }
}
