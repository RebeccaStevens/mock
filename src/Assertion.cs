using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using uMod.Common;

namespace uMod.Mock
{
    [DebuggerStepThrough]
    [DebuggerNonUserCode]
    public class Assertion
    {
        public class AssertTimer
        {
            private readonly Assertion _assertion;

            public AssertTimer(Assertion assertion)
            {
                _assertion = assertion;
            }

            public void IsCompleted(ITimer timer)
            {
                Assert.AreNotEqual(-1, _assertion.Await.Timer(timer));
            }
        }

        public class AssertPromise
        {
            private readonly Assertion _assertion;

            public AssertPromise(Assertion assertion)
            {
                _assertion = assertion;
            }

            public bool Rejected(IPromise promise, int tries = 50, int sleep = 25)
            {
                bool result = false;
                _assertion.Await.IsTrue(delegate
                {
                    bool settled = false;
                    promise.ThenPeek(
                        () => settled = true,
                        (ex) =>
                        {
                            result = true;
                            settled = true;
                        }
                    );
                    return settled;
                }, tries, sleep);
                return result;
            }

            public bool Fulfilled(IPromise promise, int tries = 50, int sleep = 25)
            {
                bool result = false;
                _assertion.Await.IsTrue(delegate
                {
                    bool settled = false;
                    promise.ThenPeek(
                        () =>
                        {
                            result = true;
                            settled = true;
                        },
                        (ex) => settled = true
                    );
                    return settled;
                }, tries, sleep);
                return result;
            }
        }

        public class AssertAwait
        {
            // IMPORTANT: TWEAK THESE IF TIMER TESTS SEEMINGLY FAIL FOR NO REASON
            // These multipliers determine how long the tests will wait for a timer result
            private const int DelayMultiplier = 25;

            private const int RepeatMultiplier = 3;

            private readonly Assertion _assertion;

            public AssertAwait(Assertion assertion)
            {
                _assertion = assertion;
            }

            public bool Log(string logString, int maxTries = 50, int sleep = 25)
            {
                int tries = 0;
                while (!BaseTest.GetLog().Contains(logString))
                {
                    if (tries > maxTries)
                    {
                        break;
                    }

                    System.Threading.Thread.Sleep(sleep);
                    tries++;
                }

                return BaseTest.GetLog().Contains(logString);
            }

            public void IsLogged(string logString, int tries = 50, int sleep = 25)
            {
                Assert.IsTrue(Log(logString, tries, sleep), $"IsLogged(\"{logString}\")");
            }

            public void IsNotLogged(string logString, int tries = 50, int sleep = 25)
            {
                Assert.IsFalse(Log(logString, tries, sleep), $"IsNotLogged(\"{logString}\")");
            }

            public void IsFalse(Func<bool> callback, int tries = 50, int sleep = 25, string message = null)
            {
                float start = _assertion.TestScope.Module.Now;
                float lastCall = start;
                int rounds = 1;
                bool result;
                while (result = callback.Invoke())
                {
                    if (rounds > tries)
                    {
                        break;
                    }

                    System.Threading.Thread.Sleep(sleep);
                    float now = _assertion.TestScope.Module.Now;
                    _assertion.TestScope.Module.OnFrame(now - lastCall);
                    lastCall = now;
                    rounds++;
                }

                Assert.IsFalse(result, message);
            }

            public void IsTrue(Func<bool> callback, int tries = 50, int sleep = 25, string message = null)
            {
                float start = _assertion.TestScope.Module.Now;
                float lastCall = start;
                int rounds = 1;
                bool result;
                while ((result = callback.Invoke()) == false)
                {
                    if (rounds > tries)
                    {
                        break;
                    }

                    System.Threading.Thread.Sleep(sleep);
                    float now = _assertion.TestScope.Module.Now;
                    _assertion.TestScope.Module.OnFrame(now - lastCall);
                    lastCall = now;
                    rounds++;
                }

                Assert.IsTrue(result, message);
            }

            public float Timer(ITimer timer)
            {
                float start = _assertion.TestScope.Module.Now;
                float lastCall = start;

                float delay = (timer.Delay > 0) ? timer.Delay * DelayMultiplier : DelayMultiplier;
                int multiplier = (timer.Repetitions > 0) ? timer.Repetitions * RepeatMultiplier : RepeatMultiplier;

                for (int i = 0; i <= (multiplier * delay); i++)
                {
                    System.Threading.Thread.Sleep(100);
                    _assertion.TestScope.Module.OnFrame(_assertion.TestScope.Module.Now - lastCall);
                    if (timer.Destroyed)
                    {
                        return (_assertion.TestScope.Module.Now - start);
                    }
                }

                return -1;
            }
        }

        protected TestScope TestScope;
        public AssertTimer Timer;
        public AssertPromise Promise;
        public AssertAwait Await;

        public Assertion(TestScope testScope)
        {
            TestScope = testScope;
            Timer = new AssertTimer(this);
            Promise = new AssertPromise(this);
            Await = new AssertAwait(this);
        }

        public void AreEqual<T>(T expected, T actual)
        {
            Assert.AreEqual(expected, actual);
        }

        public void AreEqual(System.String expected, System.String actual, System.Boolean ignoreCase)
        {
            Assert.AreEqual(expected, actual, ignoreCase);
        }

        public void AreEqual(System.Single expected, System.Single actual, System.Single delta)
        {
            Assert.AreEqual(expected, actual, delta);
        }

        public void AreEqual(System.Double expected, System.Double actual, System.Double delta, System.String message, params System.Object[] parameters)
        {
            Assert.AreEqual(expected, actual, delta, message, parameters);
        }

        public void AreEqual(System.Object expected, System.Object actual, System.String message, params System.Object[] parameters)
        {
            Assert.AreEqual(expected, actual, message, parameters);
        }

        public void AreEqual(System.Single expected, System.Single actual, System.Single delta, System.String message, params System.Object[] parameters)
        {
            Assert.AreEqual(expected, actual, delta, message, parameters);
        }

        public void AreEqual<T>(T expected, T actual, System.String message, params System.Object[] parameters)
        {
            Assert.AreEqual(expected, actual, message, parameters);
        }

        public void AreEqual(System.Double expected, System.Double actual, System.Double delta)
        {
            Assert.AreEqual(expected, actual, delta);
        }

        public void AreEqual(System.Object expected, System.Object actual)
        {
            Assert.AreEqual(expected, actual);
        }

        public void AreEqual(System.String expected, System.String actual, System.Boolean ignoreCase, System.Globalization.CultureInfo culture)
        {
            Assert.AreEqual(expected, actual, ignoreCase, culture);
        }

        public void AreEqual(System.String expected, System.String actual, System.Boolean ignoreCase, System.Globalization.CultureInfo culture, System.String message, params System.Object[] parameters)
        {
            Assert.AreEqual(expected, actual, ignoreCase, culture, message, parameters);
        }

        public void AreEqual(System.String expected, System.String actual, System.Boolean ignoreCase, System.String message, params System.Object[] parameters)
        {
            Assert.AreEqual(expected, actual, ignoreCase, message, parameters);
        }

        public void AreNotEqual(System.String notExpected, System.String actual, System.Boolean ignoreCase, System.Globalization.CultureInfo culture)
        {
            Assert.AreNotEqual(notExpected, actual, ignoreCase, culture);
        }

        public void AreNotEqual(System.String notExpected, System.String actual, System.Boolean ignoreCase, System.Globalization.CultureInfo culture, System.String message, params System.Object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, ignoreCase, culture, message, parameters);
        }

        public void AreNotEqual(System.Single notExpected, System.Single actual, System.Single delta)
        {
            Assert.AreNotEqual(notExpected, actual, delta);
        }

        public void AreNotEqual(System.String notExpected, System.String actual, System.Boolean ignoreCase)
        {
            Assert.AreNotEqual(notExpected, actual, ignoreCase);
        }

        public void AreNotEqual(System.Double notExpected, System.Double actual, System.Double delta)
        {
            Assert.AreNotEqual(notExpected, actual, delta);
        }

        public void AreNotEqual(System.Double notExpected, System.Double actual, System.Double delta, System.String message, params System.Object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, delta, message, parameters);
        }

        public void AreNotEqual(System.Single notExpected, System.Single actual, System.Single delta, System.String message, params System.Object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, delta, message, parameters);
        }

        public void AreNotEqual(System.String notExpected, System.String actual, System.Boolean ignoreCase, System.String message, params System.Object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, ignoreCase, message, parameters);
        }

        public void AreNotEqual<T>(T notExpected, T actual)
        {
            Assert.AreNotEqual(notExpected, actual);
        }

        public void AreNotEqual<T>(T notExpected, T actual, System.String message, params System.Object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, message, parameters);
        }

        public void AreNotEqual(System.Object notExpected, System.Object actual, System.String message, params System.Object[] parameters)
        {
            Assert.AreNotEqual(notExpected, actual, message, parameters);
        }

        public void AreNotEqual(System.Object notExpected, System.Object actual)
        {
            Assert.AreNotEqual(notExpected, actual);
        }

        public void AreNotSame(System.Object notExpected, System.Object actual)
        {
            Assert.AreNotSame(notExpected, actual);
        }

        public void AreNotSame(System.Object notExpected, System.Object actual, System.String message, params System.Object[] parameters)
        {
            Assert.AreNotSame(notExpected, actual, message, parameters);
        }

        public void AreSame(System.Object expected, System.Object actual)
        {
            Assert.AreSame(expected, actual);
        }

        public void AreSame(System.Object expected, System.Object actual, System.String message, params System.Object[] parameters)
        {
            Assert.AreSame(expected, actual, message, parameters);
        }

        public new void Equals(System.Object objA, System.Object objB)
        {
            Assert.Equals(objA, objB);
        }

        public void Fail()
        {
            Assert.Fail();
        }

        public void Fail(System.String message, params System.Object[] parameters)
        {
            Assert.Fail(message, parameters);
        }

        public void Inconclusive(System.String message, params System.Object[] parameters)
        {
            Assert.Inconclusive(message, parameters);
        }

        public void Inconclusive()
        {
            Assert.Inconclusive();
        }

        public void IsFalse(System.Boolean condition)
        {
            Assert.IsFalse(condition);
        }

        public void IsFalse(System.Boolean condition, System.String message, params System.Object[] parameters)
        {
            Assert.IsFalse(condition, message, parameters);
        }

        public void IsInstanceOfType(System.Object value, System.Type expectedType)
        {
            Assert.IsInstanceOfType(value, expectedType);
        }

        public void IsInstanceOfType(System.Object value, System.Type expectedType, System.String message, params System.Object[] parameters)
        {
            Assert.IsInstanceOfType(value, expectedType, message, parameters);
        }

        public void IsNotInstanceOfType(System.Object value, System.Type wrongType, System.String message, params System.Object[] parameters)
        {
            Assert.IsNotInstanceOfType(value, wrongType, message, parameters);
        }

        public void IsNotInstanceOfType(System.Object value, System.Type wrongType)
        {
            Assert.IsNotInstanceOfType(value, wrongType);
        }

        public void IsNotNull(System.Object value, System.String message, params System.Object[] parameters)
        {
            Assert.IsNotNull(value, message, parameters);
        }

        public void IsNotNull(System.Object value)
        {
            Assert.IsNotNull(value);
        }

        public void IsNull(System.Object value, System.String message, params System.Object[] parameters)
        {
            Assert.IsNull(value, message, parameters);
        }

        public void IsNull(System.Object value)
        {
            Assert.IsNull(value);
        }

        public void IsTrue(System.Boolean condition, System.String message, params System.Object[] parameters)
        {
            Assert.IsTrue(condition, message, parameters);
        }

        public void IsTrue(System.Boolean condition)
        {
            Assert.IsTrue(condition);
        }

        public void ReplaceNullChars(System.String input)
        {
            Assert.ReplaceNullChars(input);
        }
    }
}
