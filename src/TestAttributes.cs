﻿using System;

namespace uMod.Mock
{
    /// <summary>
    /// Specify Environment variable
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class EnvironmentAttribute : Attribute
    {
        public string Name;
        public string Value;

        public EnvironmentAttribute(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }

    /// <summary>
    /// Specify plugin to schedule for compilation
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class CompileAttribute : Attribute
    {
        public string Root = string.Empty;
        public string[] Paths;

        public CompileAttribute(params string[] paths)
        {
            Paths = paths;
        }
    }

    /// <summary>
    /// Loads the specified plugins for the test method or test class
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class PluginAttribute : Attribute
    {
        public Type[] PluginTypes;

        public PluginAttribute(params Type[] pluginTypes)
        {
            PluginTypes = pluginTypes;
        }
    }

    /// <summary>
    /// Loads the specified extensions for the test method or test class
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ExtensionAttribute : Attribute
    {
        public Type[] ExtensionTypes;

        public ExtensionAttribute(params Type[] extensionTypes)
        {
            ExtensionTypes = extensionTypes;
        }
    }

    /// <summary>
    /// Loads the specified libraries for the test method or test class
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class LibraryAttribute : Attribute
    {
        public Type[] LibraryTypes;

        public LibraryAttribute(params Type[] libraryTypes)
        {
            LibraryTypes = libraryTypes;
        }
    }

    /// <summary>
    /// Instructs core that a particular app is required
    /// Plugin
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ApplicationAttribute : Attribute
    {
        public string[] AppNames;

        public ApplicationAttribute(params string[] appNames)
        {
            AppNames = appNames;
        }
    }

    public enum ServerScope
    {
        PerTest,
        PerMethod,
    }

    /// <summary>
    /// Instructs test to rebuild server on a per-test or per-method basis
    /// Plugin
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ServerAttribute : Attribute
    {
        public ServerScope ServerScope;

        public ServerAttribute(ServerScope serverScope)
        {
            ServerScope = serverScope;
        }
    }
}
