using System;
using System.Globalization;
using System.Net;
using uMod.Common;

namespace uMod.Mock
{
    public class MockServer : IServer
    {
        public static MockServerPlayer ServerPlayer;

        public string Name
        {
            get => "Mock Server";
            set
            {
            }
        }

        public IPAddress Address { get; } = IPAddress.Any;

        public IPAddress LocalAddress { get; } = IPAddress.Any;

        public ushort Port => 0;

        public string Version => string.Empty;

        public string Protocol => string.Empty;

        public CultureInfo Language => CultureInfo.CurrentCulture;

        public int Players => 0;

        public int MaxPlayers
        {
            get => 0;
            set
            {
            }
        }

        public DateTime Time
        {
            get => DateTime.Now;
            set
            {
            }
        }

        public int FrameRate => 0;

        public int TargetFrameRate
        {
            get => 0;
            set
            {
            }
        }

        public ISaveInfo SaveInfo { get; } = new SaveInfo();
        public IPlayerManager PlayerManager { get; set; }

        public MockServer()
        {
            ServerPlayer = new MockServerPlayer();
        }

        public void Ban(string id, string reason, TimeSpan duration = default)
        {
            IPlayer player = PlayerManager?.FindPlayer(id);
            player?.Ban(reason, duration);
        }

        public TimeSpan BanTimeRemaining(string id)
        {
            IPlayer player = PlayerManager?.FindPlayer(id);
            return player?.BanTimeRemaining ?? TimeSpan.Zero;
        }

        public void Broadcast(string message, string prefix, params object[] args)
        {
            Console.WriteLine($"{prefix}: {string.Format(message, args)}");
        }

        public void Broadcast(string message, string prefix, ulong id, params object[] args)
        {
            Console.WriteLine($"{prefix}: {string.Format(message, args)}");
        }

        public void Broadcast(string message, ulong id, params object[] args)
        {
            Console.WriteLine(message, args);
        }

        public void Broadcast(string message)
        {
            Console.WriteLine(message);
        }

        public void Command(string command, params object[] args)
        {
            string fullCommand = (args != null && args.Length > 0) ? $"{command} {string.Join(" ", args)}" : command;
            MockProvider.CommandSystem.Handler.HandleConsoleMessage(ServerPlayer, fullCommand);
        }

        public bool IsBanned(string id)
        {
            IPlayer player = PlayerManager?.FindPlayer(id);
            return player?.IsBanned ?? false;
        }

        public void Save()
        {
        }

        public void Shutdown(bool save = true, int delay = 0)
        {
        }

        public void Unban(string id)
        {
            IPlayer player = PlayerManager?.FindPlayer(id);
            player?.Unban();
        }
    }
}
