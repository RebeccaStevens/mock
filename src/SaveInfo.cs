﻿using System;
using uMod.Common;

namespace uMod.Mock
{
    class SaveInfo : ISaveInfo
    {
        public string SaveName { get; private set; }

        public DateTime CreationTime { get; private set; }

        public uint CreationTimeUnix { get; private set; }

        public void Refresh()
        {
        }
    }
}
